package com.hugoibm.restAppTest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hugoibm.restAppTest.business.QuestionBusiness;
import com.hugoibm.restAppTest.model.Question;

@RestController
public class QuestionController {
	
	@Autowired QuestionBusiness questionBussines;
	
	@GetMapping("/All-Questions")
	public List<Question> getAllQuestions(){
		//List<Question> listQuestions = Arrays.asList(new Question(1,"What is OOP","Oriented Object Progaming","None"), new Question(2,"New","Test1",""));
		//return listQuestions;
		return questionBussines.getAllQuestions();
	}
	
	@PostMapping("/Add-Question")
	public boolean addQuestion(@RequestBody Question question) {
		System.out.println("Add-Question question:"+ question.toString());
		return true;
	}
	
	@PatchMapping("/Update-Question-Value/{questionId}/{valueName}")
	public boolean updateQuestionValue(@PathVariable("questionId") Integer questionId, @PathVariable("valueName") String valueName, @RequestBody Question question ) {
		System.out.println("Update-Question-Value questionId:"+questionId+" valueName:"+valueName+" Question:"+question.toString());
		return true;
	}
	
	@PutMapping("/Update-Question/{questionId}/")
	public boolean updateQuestion(@PathVariable("questionId") Integer questionId, @RequestBody Question question ) {
		System.out.println("Update-Question questionId:"+questionId+" Question:"+question.toString());
		return true;
	}
	
	@DeleteMapping("Delete-Question/{questionId}")
	public boolean deleteQuestion(@PathVariable("questionId") Integer questionId) {
		System.out.println("Delete-Question questionId:"+questionId);
		return true;
	}
}
