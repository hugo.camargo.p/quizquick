package com.hugoibm.restAppTest.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hugoibm.restAppTest.business.QuizBusiness;
import com.hugoibm.restAppTest.model.Quiz;

@RestController
public class QuizController {
	
	@Autowired QuizBusiness quizBusiness;
	
	@GetMapping("/all-quiz")
	public List<Quiz> getAllQuiz(){
		return quizBusiness.getAllQuiz();
	}

	@GetMapping("/quiz/{quizId}")
	public Optional<Quiz> getQuizById(@PathVariable Integer quizId) {
		return quizBusiness.getQuizById(quizId);
	}

	@GetMapping("/quiz/name/{quizName}")
	public Optional<Quiz> getQuizByQuizName(@PathVariable String quizName) {
		return quizBusiness.getQuizByQuizName(quizName);
	}
	
	@GetMapping("/quiz/testQueryParam")
	public String getQueryParam(@RequestParam("appVersion") String appVersion) {
		return appVersion;
	}
}
