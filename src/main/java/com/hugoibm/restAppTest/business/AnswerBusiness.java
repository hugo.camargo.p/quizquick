package com.hugoibm.restAppTest.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hugoibm.restAppTest.data.IAnswerRepository;
import com.hugoibm.restAppTest.model.Answer;

@Component
public class AnswerBusiness {

	@Autowired
	IAnswerRepository iAnswerRepository;

	public List<Answer> getAllAnswer() {
		return iAnswerRepository.findAll();
	}

	public List<Answer> getAnswerByQuestion(Integer questionId) {
		return iAnswerRepository.getAnswerByQuestionId(questionId);
	}
}
