package com.hugoibm.restAppTest.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hugoibm.restAppTest.data.IQuestionRepository;
import com.hugoibm.restAppTest.model.Question;

@Component
public class QuestionBusiness {

	@Autowired IQuestionRepository questionRepository;
	
	public List<Question> getAllQuestions(){
		return questionRepository.findAll();
	}
}
