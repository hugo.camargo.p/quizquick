package com.hugoibm.restAppTest.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@WebMvcTest(QuestionController.class)
public class QuestionControllerTest {
	
	//Loading Mvc module to work with spring mock
	@Autowired 
	private MockMvc mockMvc;
	
	@Test
	public void getAllQuestions_test_1() throws Exception {
		
		 RequestBuilder request = MockMvcRequestBuilders
				 .get("/All-Questions")
				 .accept(MediaType.APPLICATION_JSON);
		 
		 MvcResult result = mockMvc.perform(request)
		 		.andExpect(status().isOk())
		 		.andExpect(content().json("[{\"id\":1,\"question\":\"What is OOP\",\"response\":\"Oriented Object Progaming\",\"image\":\"None\",\"value\":0},{\"id\":2,\"question\":\"New\",\"response\":\"Test1\",\"image\":\"\",\"value\":0}]"))
		 		.andReturn();
		
		 System.out.println(result.getResponse().getContentAsString());
		
		  //MvcResult result = (MvcResult) mockMvc.perform(request);
		  //System.out.println(result.getResponse());
	}
}
